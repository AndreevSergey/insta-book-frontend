import { Component, HostListener, Injectable, OnInit } from '@angular/core'
import { HttpApiService } from './services/http-api/http-api.service'
import { sendSelectPostService } from './services/sendSelectPost/sendSelectPostService'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
@Injectable()
export class AppComponent implements OnInit {
  title = 'app'
  public arrPost = []
  public arrPostActive = []
  public arrSendPost = []
  public valueNick = ''
  key_enter = 13

  public constructor(private api: HttpApiService, public sendPosts: sendSelectPostService) {}

  async ngOnInit() {
    this.arrSendPost = []
    this.arrPostActive.length = this.arrPost.length
  }

  selectPost(i) {
    if (this.arrPostActive[i] == true) {
      this.arrPostActive[i] = false
      const indexItemForDelete = this.arrSendPost.indexOf({
        likeCount: this.arrPost[i].likeCount,
        caption: this.arrPost[i].caption,
        imageUrl: this.arrPost[i].imageUrl,
        date: this.arrPost[i].date
      })
      this.arrSendPost.splice(indexItemForDelete, 1)
    } else {
      this.arrPostActive[i] = true
      this.arrSendPost.push({
        likeCount: this.arrPost[i].likeCount,
        caption: this.arrPost[i].caption,
        imageUrl: this.arrPost[i].imageUrl,
        date: this.arrPost[i].date
      })
    }
    console.log(this.arrSendPost)
  }

  resetSelectPost() {
    this.arrSendPost = []
    this.arrPostActive.forEach((item, id) => {
      this.arrPostActive[id] = false
    })
  }
  async sendSelectPost() {
    const send: any = await this.sendPosts.send({ data: this.arrSendPost })
  }

  async valueNickchange() {
    this.resetSelectPost()
    if (this.valueNick !== '') {
      const getPost = await this.api.get<any>('photos?username=' + this.valueNick);
      this.arrPost = getPost.data;
      this.arrPostActive.length = this.arrPost.length;
        this.valueNick = '';
      alert('Посты в обработке, после будут отправлены к вам на почту, ожидайте');
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === this.key_enter) {
      this.enter_input()
    }
  }

  enter_input() {
    this.valueNickchange()
  }
}
