import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpApiService} from './services/http-api/http-api.service';
import {HttpClientModule} from '@angular/common/http';
import {sendSelectPostService} from "./services/sendSelectPost/sendSelectPostService";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
      FormsModule
  ],
  providers: [
    HttpApiService,
    sendSelectPostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
